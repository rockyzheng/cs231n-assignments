#!/usr/bin/env python
# -*-coding:utf-8-*-

import data_utils
from classifiers.softmax import Softmax
import numpy as np
from sklearn.metrics.classification import classification_report
from matplotlib import pyplot as plt

DATA_ROOT = "../datasets/cifar-10-batches-py"

X_train, Y_train, X_test, Y_test = data_utils.load_CIFAR(DATA_ROOT)
X_train = X_train.reshape((X_train.shape[0], -1))
X_test = X_test.reshape((X_test.shape[0], -1))

num_class = np.max(Y_train) + 1
num_feature = X_train.shape[1]
cls = Softmax(num_feature, num_class)
loss_history = cls.train(X_train, Y_train, verbos=True, max_iters = 1000, reg = 10)

plt.plot(loss_history)
plt.show()

y_pred = cls.predict(X_test)

print(y_pred.shape)
print(Y_test.shape)
print(classification_report(list(Y_test), list(y_pred)))