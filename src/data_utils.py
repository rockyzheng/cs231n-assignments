#!/usr/bin/env python
#-*-coding:utf-8-*-


import os
import numpy as np
import platform
import pickle


def load_pickle(f):
    version = platform.python_version_tuple()
    if version[0] == '2':
        return pickle.load(f)
    elif version[0] == '3':
        return pickle.load(f, encoding='latin1')
    raise ValueError("invalid python version: {}".format(version))


def load_CIFAR_batch(filename):

    with open(filename, 'rb') as f:
        datadict = load_pickle(f)
        X = datadict['data']
        Y = datadict['labels']
        X = X.reshape(10000, 3, 32, 32).transpose(0, 2, 3, 1).astype("float")
        Y = np.array(Y)
        return X, Y


def load_CIFAR(root):
    xs = []
    ys = []
    for i in range(1, 6):
        filename = os.path.join(root, "data_batch_%d"%(i))
        X, Y = load_CIFAR_batch(filename)
        xs.append(X)
        ys.append(Y)

    X_train = np.concatenate(xs)
    Y_train = np.concatenate(ys)
    X_test , Y_test = load_CIFAR_batch(os.path.join(root, "test_batch"))
    return X_train, Y_train, X_test, Y_test