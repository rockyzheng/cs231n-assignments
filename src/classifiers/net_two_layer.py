#!/usr/bin/env python
#-*-coding:utf-8-*-


import numpy as np

from src.classifiers.utils import init_weights_random

class NetTwoLayer(object):

    def __init__(self, input_size, hidden_size, output_size):

        self.input_size = input_size
        self.hidden_size = hidden_size
        self.output_size = output_size

        self.W1 = init_weights_random(input_size, hidden_size)
        self.b1 = init_weights_random(1, hidden_size)
        self.W2 = init_weights_random(hidden_size, output_size)
        self.b2 = init_weights_random(1, output_size)
        self.N = -1


    def train(self, X, Y, lr = 1e-7, reg=1e-5, max_iters=100, batch_size=100, verbos=True):
        loss_history = []
        self.N = X.shape[0]
        for iter in range(max_iters):
            # todo batch
            xmask = np.random.choice(np.arange(self.N), batch_size)
            x_batch = X[xmask]
            y_batch = Y[xmask]
            
            loss, grad = self.forward(x_batch, y_batch, reg)
            self.W2 -= lr * grad['dw2']
            self.b1 -= lr * grad['db1']
            self.W1 -= lr * grad['dw1']
            self.b1 -= lr * grad['db1']
            loss_history.append(loss)
            if verbos:
                print("%d iteration loss: %f "%(iter, loss))
        return loss_history

    

    def forward(self, x, y, reg):
        grad = {}
        sample_num = x.shape[0]

        z_1 = x.dot(self.W1) + self.b1
        a_1 = np.maximum(0, z_1)   # reul

        z_2 = a_1.dot(self.W2) + self.b2

        z_2 -= np.max(z_2, axis=1).reshape(-1, 1)
        
        output_p = np.exp(z_2) /  np.sum(np.exp(z_2), axis=1).reshape(-1, 1)
        
        # todo 正则怎么加
        loss = np.mean(-1 * np.log(output_p[np.arange(sample_num), y])) 
        loss += 0.5 * reg * np.sum(self.W1* self.W1) + np.sum(self.W2 * self.W2)

        output_p[np.arange(sample_num), y] -= 1   # (N, C)

        dz2 = output_p

        dw2 = a_1.T.dot(dz2)                 #(N, H).T (N,C)    , (H, C)
        db2 = np.sum(dz2, axis=1)
        
        dz1 = dz2.dot(self.W2.T)  #(N, C)   (H, C)= (N, H)
        dz1 = dz1 * a_1
        dw1 = x.T.dot(dz1) # (N, H),
        db1 = np.sum(dz1, axis=1)

        grad["dw1"] = dw1
        grad['db1'] = db1
        grad['dw2'] = dw2
        grad['db2'] = db2
        
        return loss, grad
    
        
        
    def predict(self, x):
        z_1 = x.dot(self.W1) + self.b1
        a_1 = np.maximum(0, z_1)  # reul

        z_2 = a_1.dot(self.W2) + self.b2

        z_2 -= np.max(z_2, axis=1).reshape(-1, 1)
        output_p = np.exp(z_2) / np.sum(np.exp(z_2), axis=1).reshape(-1, 1)
        return np.argmax(output_p, axis=1)
        
