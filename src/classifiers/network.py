#!/usr/bin/env python
#-*-coding:utf-8-*-

import numpy as np

from src.classifiers import utils

class Active(object):
    
    def __init__(self):
        pass
    
    def active(self):
        raise NotImplementedError("active not implement ")
    
    def dreactive(self, z):
        raise  NotImplementedError("dreactive not implement ")

class Sigmoid(Active):
    
    def active(self, z):
        return  1.0 / (1 + np.exp(-z))
    
    def dreactive(self, z):
        return self.active(z) * (1- self.active())


class Reul(Active):
    
    def active(self, z):
        return np.maximum(np.zeros(z.shape), z)
    
    

class NetWork(object):
    def __init__(self, sizes, active):
        self.sizes = sizes
        self.weights = []
        self.bias = []
        self.active = active

        for row, col  in zip(sizes[:-1], sizes[1: ]):
            self.weights.append(utils.init_weights_random(row, col))
            self.bias.append(utils.init_weights_random(1, col))
    
    
    def train(self, x, y , lr=1e-4, reg=1e-5):
        pass
    
    
    def forward(self, x, y):
        Z = []
        A = []
        
        z_0 = x.dot(self.weights[0])
        Z.append(z_0)
        A.append(self.active.active(z_0))
        
        for i, w in enumerate(self.weights[1:]):
            z = Z[i-1].dot(w)
            Z.append(z)
            a = self.active.active(z)
            A.append(a)
        
        
    
    def backprob(self):
        
        pass


if __name__ == '__main__':
    net = NetWork([100, 30, 10])
    









