#!/usr/bin/env python
# -*-coding:utf-8-*-

import numpy as np


def init_weights_random(row, col):
    weight = 0.0001* np.random.randn(row, col)
    return weight


class Softmax():
    def __init__(self, feature_dim, num_class):
        self.W = init_weights_random(feature_dim, num_class)
        self.b = np.random.randn(num_class) * 0.0001
        self.C = num_class
        self.D = feature_dim


    def train(self, X, Y, lr=2e-7, reg=0.005, max_iters=100, batch_size=500, verbos= False):
        self.N = X.shape[0]
        loss_history = []
        
        for iter in range(max_iters):
            # todo rand choice
            xmask = np.random.choice(np.arange(self.N), batch_size)
            x_batch = X[xmask]
            y_batch = Y[xmask]
            
            ## no batch , all sample training
            # loss, dW, db = self._train_batch(X, Y, reg)
            loss, dW, db = self._train_batch(x_batch, y_batch, reg)
            self.W -= lr * dW
            self.b -= lr * db
            
            if verbos:
                print("%d iteration loss:%f "%(iter, loss))
            loss_history.append(loss)
        return loss_history


    def _train_batch(self, x_batch, y_batch, reg):
        batch_size = x_batch.shape[0]
        z = x_batch.dot(self.W)  + self.b
        z -= np.max(z, axis=1, keepdims=True)
        softmax_output = np.exp(z) / np.sum(np.exp(z), axis=1).reshape((batch_size, 1))

        log_likihood = -1 * np.sum(np.log(softmax_output[np.arange(batch_size), y_batch]))
        loss = log_likihood + reg * np.sum(self.W * self.W)
        loss /= batch_size
        
        softmax_output[np.arange(batch_size), y_batch] -= 1
        db = np.sum(softmax_output, axis = 0) / batch_size
        dW = x_batch.T.dot(softmax_output) / batch_size
        dW += reg * self.W
        return loss, dW, db


    def predict(self, X):
        z = X.dot(self.W) + self.b
        z -= np.max(z, axis=1, keepdims=True)
        softmax_output = np.exp(z) / np.sum(np.exp(z), axis=1).reshape((-1, 1))
        # print(softmax_output[0])
        return np.argmax(softmax_output, axis=1)
