#!/usr/bin/env python
#-*-coding:utf-8-*-


import numpy as np


def init_weights_random(row, col):
    weight = 0.0001* np.random.randn(row, col)
    return weight
